Monocle sample app

**В ДАННЫЙ МОМЕНТ РЕПОЗИТОРИЙ ЯВЛЯЕТСЯ УСТАРЕВШИМ. Рекомендуется использовать автоматическое создание модуля сборщиком: https://bitbucket.org/langprisminternal/django-monocle**
==================

Проект предназначен для работы на языке Python 3.X и выше.
Это демо-модуль созданный в качестве примера для разработки других модулей для сборщика одностраничных сайтов django-monocle. Подробнее о django-monocle - https://bitbucket.org/langprism/django-monocle .
Модуль устанавливается сборщиком с помощью менеджера pip3, поэтомоу должен быть собран и упакован с помощью него.

#. Структура папок модуля
#. Конфигурационный файл
#. json файл импорта
#. Примеры файлов (Модели и Админка)
#. Публикация через pip3
#. Внесение изменений в модули


**Структура папок модуля**

Модуль представляет собой стандартное приложение для django и включает в себя модели, средства генерации систем управления, шаблоны, статические файлы, а также файлы для интеграции и сборки.
Так как модуль встраивается в одностраничный сайт с помощью сборщика, то у него нет файлов view.py и urls.py. Для передачи данных из модуля в основной проект используется файл mionocle.py содеражащий нужные вызовы для определенного модуля.
Модули являются частями проекта автоматической сборки одностраничных сайтов на django - https://bitbucket.org/langprism/django-monocle.
Необходимо соблюдать следующие условия наименования файлов и папок.

* monocle_{{PROJECT_NAME}}
    * templates
        * monocle_{{PROJECT_NAME}}
            *monocle_{{PROJECT_NAME}}.html
    * static
        * monocle_{{PROJECT_NAME}}
            * assets
            * monocle_{{PROJECT_NAME}}.css
            * monocle_{{PROJECT_NAME}}.js
    * fixtures
        *monocle_{{PROJECT_NAME}}
            [Картинки]
        *monocle_{{PROJECT_NAME}},json
    * models.py
    * admin.py
    * monocle.py   
* MANIFEST.in
* README.rst
* setup.py
* reqs.txt

**Конфигурационный файл**

Для сборки и интеграции проекта сборщик django-monocle использует файл модуля monocle.py: ::


    # название модуля - должно совпадать с {{PROJECT_NAME}}
    appname = 'monocle_sample'

    # модели импортируемые в основное приложение одностраничного проекта
    models = ['SampleModel']

    """
        строка передающая данные из модели в контекст основного шаблона. Этот вызов используется во view.py файле основого приложения проекта при сборке.
    """
    context_callback =  "'monocle_sample_models': SampleModel.objects.all().filter(isShown=True)"

   included_app_reqs = [ ] - зависимости, подключаемые в файле settings.py проекта.

**json файл импорта (пример)** ::

	Пример 1:
    [
    {
     "model": "monocle_partners.partner",
     "fields": {
      "position": 0,
      "image": "monocle_partners/image.jpg",
      "name": "Партнер1",
      "isShown": true
     },
     "pk": 1
    },
    {
     "model": "monocle_partners.partner",
     "fields": {
      "position": 2,
      "image": "monocle_partners/image.jpg",
      "name": "Партнер2",
      "isShown": true
     },
     "pk": 2
    }
    ]

    Пример 2:
    [
    {"pk": 1, "model": "monocle_slider.slider", "fields": {"text": "Описание для карусели", "isShown": true, "elem_number": 3, "arrows": "true", "name": "Карусель", "position": 0, "pagination": "true"}},
    {"pk": 1, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 1", "isShown": true, "image": "monocle_slider/Chrysanthemum.jpg", "slider": 1, "name": "Слайд 1", "position": 0}},
    {"pk": 2, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 2", "isShown": true, "image": "monocle_slider/Desert.jpg", "slider": 1, "name": "Слайд 2", "position": 1}},
    {"pk": 3, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 3", "isShown": true, "image": "monocle_slider/Koala.jpg", "slider": 1, "name": "Слайд 3", "position": 2}},
    {"pk": 4, "model": "monocle_slider.slide", "fields": {"text": "Описание для слайда 4", "isShown": true, "image": "monocle_slider/Penguins.jpg", "slider": 1, "name": "Слайд 4", "position": 3}}
    ]
**Примеры файлов (Модели и Админка)**

# models.py ::

	from django.db import models
	from solo.models import SingletonModel

	from django.conf import settings

	CHOICES = [[app.replace("apps.", ''), app.replace("apps.", ''),] for app in settings.MY_APPS]

	class Section(SingletonModel):
		name = models.CharField(max_length=255, default='Секция с блоками сайта', verbose_name= 'Название секции с блоками')

		def __str__(self):
			return self.name
		class Meta:
			verbose_name_plural = u'Блоки'
			verbose_name = u'Блок'

	class Block(models.Model):
		name = models.CharField(max_length=255, blank=True, verbose_name= 'Название блока')
		Section = models.ForeignKey('Section', verbose_name= 'Секция')
		app_name = models.CharField(choices=CHOICES, default='true', max_length=255, verbose_name= 'Название monocle-приложения', help_text='Пример: monocle_main')
		text = models.TextField(verbose_name='Описание', blank=True)
		position = models.SmallIntegerField(default=0)
		isShown = models.BooleanField(default=True, verbose_name='отображать блок')

		def __str__(self):
			return self.name

		class Meta:
			verbose_name_plural = u'Блоки'
			verbose_name = u'Блок'
			ordering = ['position']

	class Contacts(SingletonModel):
		 phone = models.CharField(max_length=100, blank=True, verbose_name='Телефон', help_text='Введите номер телефона')
		 email = models.EmailField(verbose_name='Email для приема заказов', help_text='Введите адрес электронной почты')
		 address = models.TextField(verbose_name='Адрес', help_text='Введите адрес')

		 class Meta:
			 verbose_name = u'Контакты'
			 verbose_name_plural = u'Контакты'

	from django.apps import AppConfig
	class CustomAppConfig(AppConfig):
		name = 'apps.main'
		verbose_name = 'Основные настройки'
		
		
# admin.py ::

	from django.contrib import admin
	from .models import *
	from grappelli.forms import GrappelliSortableHiddenMixin

	from django_summernote.admin import SummernoteModelAdmin
	from django_summernote.admin import SummernoteInlineModelAdmin

	class BlockInline(GrappelliSortableHiddenMixin, admin.TabularInline, SummernoteInlineModelAdmin):
		model = Block
		sortable_field_name = "position"
		extra = 0

		fieldsets = [
			('', {'fields': ['app_name', 'name', 'isShown', 'position', ], 'classes': ['collapse']}),
		]
		list_display = ('name', 'app_name', 'text', 'position', 'isShown',)

	class SectionAdmin(SummernoteModelAdmin):
		list_display = ('name',)
		list_editable = ('name',)
		inlines = [BlockInline]

	class ContactsAdmin(SummernoteModelAdmin):
		fieldsets = [
			('Контактная информация', {'fields': ['phone', 'email', 'address'], 'classes': ['collapse']}),
		]
		list_display = ('phone', 'email', 'address')
		list_editable = ('phone', 'email', 'address')

	admin.site.register(Contacts, ContactsAdmin)
	admin.site.register(Section, SectionAdmin)


# __init.py__ ::

    default_app_config = 'apps.main.models.CustomAppConfig'

**Генерация тестовых данных**: ::

    python3 manage.py  dumpdata APPNAME --format json

Результат будет выведен в консоль. Его можно использовать в дальнейшем для создания файла импорта.


**Публикация через pip3**

Модули устанавливаются сборщиком при помощи менеджера пакетов pip3. Поэтому после внесения изменений в модуль необходимо собрать его в дистрибутив и опубликовать в pipy.
Для этого нужно отредактировать файл setup.py: ::

    setup(
        name='{{PROJECT_NAME}}',
        version='0.1.0',
        packages=['{{PROJECT_NAME}}'],
        include_package_data=True,
        install_requires=[
        "requests",
        "bcrypt",
        ],
        license='BSD License',  # example license
        description='Sample app for django-monocle project',
        long_description=README,
        author='Alexander Kalinin @Langprism LTD',
        author_email='ak@langprism.com',
        classifiers=[
            'Environment :: Web Environment',
            'Framework :: Django',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: BSD License', # example license
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            # Replace these appropriately if you are stuck on Python 2.
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.2',
            'Programming Language :: Python :: 3.3',
            'Topic :: Internet :: WWW/HTTP',
            'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ],
    )

Зависимости пакетов указываются в файле setup.py в атрибуте "install_requires" (см. пример выше).

После редактирования нужно выполнить команду: ::

    python3 setup.py register sdist bdist_wheel upload

Для публикации дистрибутива в индексе нужно ввести данные аккаунта проекта: 
логин - monoculus, 
пароль - Langprism11

**Внесение изменений в модули**

В случае если модуль необходимо доработать либо внести изменения, необходимо склонировать его с репозитория. Хранилище модулей находится по ссылке https://bitbucket.org/monoculus.
После изменений необходимо снова опубликовать проект pypi, инкрементировать номер версии и выполнить push в репозиторий.